#pragma once
#include "InterpreterException.h"
#include <iostream>

class TypeErrorException : public InterpreterException
{
public:
	TypeErrorException(std::string type);
	virtual const char* what() const noexcept;
private:
	std::string _type;
};
