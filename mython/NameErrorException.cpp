#include "NameErrorException.h"

NameErrorException::NameErrorException(std::string name){
    _name = name;
}

const char* NameErrorException::what() const noexcept
{
    std::string ret = "NameError : name '" + _name + "' is not defined";
    return ret.c_str();
}
