#include "String.h"

String::String(std::string val, std::string type) : Sequence(new std::vector<Type*>(), type)
{
    _val = val.substr(1, val.length() - 2);
}

std::string String::getVal()
{
    return _val;
}

bool String::isPrintable() const
{
    return true;
}

std::string String::toString() const
{
    return "'" + _val + "'";
}
