#include "Boolean.h"

Boolean::Boolean(bool val, std::string type) : Type(type)
{
	_val = val;
}

bool Boolean::getValue()
{
	return _val;
}

bool Boolean::isPrintable() const
{
	return true;
}

std::string Boolean::toString() const
{
	return _val == true ? "True" : "False";
}
