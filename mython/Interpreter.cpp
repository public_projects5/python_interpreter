#include "Type.h"
#include "InterpreterException.h"
#include "SyntaxException.h"
#include "TypeErrorException.h"
#include "Parser.h"
#include <iostream>

#define WELCOME "Welcome to Magshimim Python Interperter version 1.0 by "
#define YOUR_NAME "Yehuda"


int main(int argc,char **argv)
{
	std::cout << WELCOME << YOUR_NAME << std::endl;

	std::string input_string;

	// get new command from user
	std::cout << ">>> ";
	std::getline(std::cin, input_string);
	
	while (input_string != "quit()")
	{
		// parsing command
		try {
			Type* object = Parser::parseString(input_string);
			if (object->isPrintable())
				std::cout << object->toString() << std::endl;
			else
				delete object;
		}
		catch (const TypeErrorException& e)
		{
			std::cout << e.what();
		}
		catch (const InterpreterException& e)
		{
			std::cout << e.what() << std::endl;
		}
		catch (const SyntaxException& e)
		{
			std::cout << e.what() << std::endl;
		}
		// get new command from user
		std::cout << ">>> ";
		std::getline(std::cin, input_string);
	}

	return 0;
}
