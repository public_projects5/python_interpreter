#include "Parser.h"
#include <iostream>

Type* Parser::parseString(std::string str)
{
	if (str[0] == ' ' || str[0] == '\t')
		throw InterpreterException();
	if (str[0] == '[' && str[str.length() - 1] == ']')
		return createList(str);
	if (str.substr(0, 4) == "type") {
		std::string name = str.substr(4, str.length() - 4);
		std::cout << "<type '" + findType(name) + "'>\n";
		return new Void("void");;
	}
	if (str.substr(0, 3) == "del") {
		del(str.substr(4, str.length() - 2));
		Type* ret = new Void("void");
		ret->setTemp(1);
		return ret;
	}
	if (str.substr(0, 3) == "len") {
		Type* len1 = len(str.substr(4, str.length() - 3));
		return len1;
	}
	Helper::rtrim(str);
	Type* optionalRet = getVariableValue(str);
	if (optionalRet != nullptr)
		return optionalRet;
	Type* ret = getType(str);
	if (ret != nullptr)
		return ret;
	if(makeAssignment(str))
	{ 
		Type* ret = new Void("void");
		ret->setTemp(1);
		return ret;
	}
	throw SyntaxException();
}

Type* Parser::getType(std::string str) {
	if (Helper::isInteger(str)) {
		int val = std::stoi(str);
		return new Integer(val, "Integer");
	}
	if (Helper::isBoolean(str)) {
		bool val = str == "True" ? true : false;
		return new Boolean(val, "Boolean");
	}
	if (Helper::isString(str))
		return new String(str, "String");	
	return nullptr;
}

void Parser::cleanMemory()
{
	_variables.clear();
}

int Parser::deepCopy(std::string src, std::string dest)
{
	if (!isLegalVarName(src) || _variables.find(src) == _variables.end())
		return 0;
	Type* copy = *new Type*(_variables[src]);
	_variables[dest] = copy;
	return 1;
}

Type* Parser::createList(std::string& s)
{
	//[123, "aaa", 5]
	std::vector<Type*>* list = new std::vector<Type*>();
	int index = 0;
	int start = 1;
	int end = s.find(",");
	bool done = false;
	while (!done) {
		std::string curr = s.substr(start, end-1);
		Helper::trim(curr);
		s = s.substr(end + 1, s.length() - 1);
		if (curr[curr.length() - 1] == ']') {    //done?
			done = true;
			curr = curr.substr(0, curr.length() - 1);
		}
		Type* currType = getType(curr);
		if (currType == nullptr)
			throw SyntaxException();
		list->push_back(currType);
		index++;
		end = s.find(",");
	}
	return new List(list, "List");
}

std::string Parser::findType(std::string& s)
{
	std::string name = s.substr(1, s.length() - 2);
	Type* val = _variables[name];
	return val->getType();
}

void Parser::del(std::string name)
{
	_variables.erase(name);
}

Integer* Parser::len(std::string name)
{
	Integer* lenRet = new Integer(0, "Integer");
	name = name.substr(0, name.length() - 1);
	std::string type = _variables[name]->getType();
	if (type != "String" && type != "List")
		throw TypeErrorException(type);
	else if (type == "String") {
		String* str = static_cast<String*>(_variables[name]);
		lenRet = new Integer(str->getVal().length(), "Integer");
	}
	else if (type == "List") {
		List* str = static_cast<List*>(_variables[name]);
		lenRet = new Integer(str->getVal().size(), "Integer");
	}
	
	return lenRet;
}

bool Parser::isLegalVarName(std::string str)
{
	if (Helper::isDigit(str[0]))
		return false;
	for (int i = 0; i < str.length(); i++) {
		if (!(Helper::isDigit(str[0]) || Helper::isUnderscore(str[0]) || Helper::isLetter(str[0])))
			return false;
	}
}

bool Parser::makeAssignment(std::string str)
{
	Type* valType;
	if (str.substr(1, str.length() - 2).find('=') == std::string::npos)
		return false;
	Helper::rtrim(str);
	std::string name = str.substr(0, str.find('='));
	std::string valStr = str.substr(str.find('=') + 1, str.length() - 1);
	Helper::trim(name);
	Helper::trim(valStr);
	if (Helper::isList(valStr)) {
		valType = createList(valStr);
		_variables[name] = valType;
		return true;
	}
	else if (deepCopy(valStr, name) == 1) 
		return true;
	Type* val = getType(valStr);
	if (!isLegalVarName(name))
		throw SyntaxException();
	if (val == nullptr)
		throw SyntaxException();
	_variables[name] = val;
	return true;
}

Type* Parser::getVariableValue(std::string str)
{
	if (_variables.find(str) != _variables.end())
		return _variables[str];
	return nullptr;
}
