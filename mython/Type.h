#pragma once
#include <iostream>
class Type
{
public:
	Type(std::string type);
	Type(Type* other);
	bool getTemp();
	void setTemp(bool temp);
	std::string getType();
	//virtuals:
	virtual bool isPrintable()const = 0;
	virtual std::string toString()const = 0;


private:
	std::string _type;
	bool _isTemp;
};
