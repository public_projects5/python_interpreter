#include "Void.h"

Void::Void(std::string type) : Type(type)
{
}

bool Void::isPrintable() const
{
	return false;
}

std::string Void::toString() const
{
	return "Void";
}
