#pragma once
#include "Type.h"

class Boolean : public Type {
public:
	Boolean(bool val, std::string type);
	bool getValue();
	bool isPrintable()const override;
	std::string toString()const override;

private:
	bool _val;
};