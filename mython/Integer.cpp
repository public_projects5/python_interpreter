#include "Integer.h"

Integer::Integer(int val, std::string type) : Type(type)
{
	_val = val;
}

int Integer::getVal()
{
	return _val;
}

bool Integer::isPrintable() const
{
	return true;
}

std::string Integer::toString() const
{
	return std::to_string(_val);
}
