#include "List.h"

List::List(std::vector<Type*>* val, std::string type) : Sequence(val, type)
{
}

bool List::isPrintable() const
{
	return true;
}

std::string List::toString() const
{
	std::string ret = "[";
	for (int i = 0; i < _val->size(); i++) {
		if (i > 0)
			ret += ", ";
		ret += (*_val)[i]->toString();
	}
	ret += "]";
	return ret;
}
