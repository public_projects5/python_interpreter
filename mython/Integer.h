#pragma once
#include "Type.h"
#include "String"

class Integer : public Type {
public:
	Integer(int val, std::string type);
	int getVal();
	bool isPrintable()const override;
	std::string toString()const override;

private:
	int _val;
};