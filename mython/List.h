#pragma once
#include"Sequence.h"

class List : public Sequence {
public:
	List(std::vector<Type*>* val, std::string type);
	bool isPrintable()const override;
	std::string toString()const override;
};