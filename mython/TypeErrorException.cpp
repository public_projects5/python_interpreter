#include "TypeErrorException.h"
#pragma warning(disable : 4996)

TypeErrorException::TypeErrorException(std::string type) {
    _type = type;
}

const char* TypeErrorException::what() const noexcept
{
    std::string ret = "TypeError: object of type '";
    ret += _type;
    ret += "' has no len()";
    char* retC = new char[ret.length()];
    strncpy(retC, ret.c_str(), ret.length());
    return retC;
}
