#pragma once
#include "InterpreterException.h"
#include "SyntaxException.h"
#include "NameErrorException.h"
#include "TypeErrorException.h"
#include "Integer.h"
#include "Boolean.h"
#include "String.h"
#include "Type.h"
#include "Void.h"
#include "List.h"
#include "Helper.h"
#include <string>
#include <unordered_map>
#include <iostream>
#include <sstream>
#include <unordered_map>

static std::unordered_map<std::string, Type*> _variables;
class Parser
{
    
public:
    static Type* parseString(std::string str);
    static Type* getType(std::string str);
    static void cleanMemory();
    static int deepCopy(std::string src, std::string dest);
    static Type* createList(std::string& s);
    static std::string findType(std::string& s);
    static void del(std::string name);
    static Integer* len(std::string name);
private:
    
    static bool isLegalVarName(std::string str);
    static bool makeAssignment(std::string str);
    static Type* getVariableValue(std::string str);
};
