#include "Type.h"

Type::Type(std::string type)
{
    _isTemp = false;
    _type = type;
}

Type::Type(Type* other)
{
    this->_isTemp = other->getTemp();
}

bool Type::getTemp()
{
    return _isTemp;
}

void Type::setTemp(bool temp)
{
    _isTemp = temp;
}

std::string Type::getType()
{
    return _type;
}
