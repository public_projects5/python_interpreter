#pragma once
#include "Type.h"

class Void : public Type {
public:
	Void(std::string type);
	bool isPrintable()const override;
	std::string toString()const override;

private:
};