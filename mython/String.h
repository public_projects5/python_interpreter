#pragma once
#include "Sequence.h"

class String : public Sequence {
public:
	String(std::string val, std::string type);
	std::string getVal();
	bool isPrintable()const override;
	std::string toString()const override;

private:
	std::string _val;
};