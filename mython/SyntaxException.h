#pragma once
#include "InterpreterException.h"
class SyntaxException : InterpreterException {
public:
	virtual const char* what() const noexcept;
};