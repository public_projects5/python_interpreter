#pragma once
#include "Type.h"
#include<vector>

class Sequence : public Type {
public:
	Sequence(std::vector<Type*>* val, std::string type);
	virtual bool isPrintable()const = 0;
	virtual std::string toString()const = 0;
	std::vector<Type*> getVal();

protected:
	std::vector<Type*> *_val;
};